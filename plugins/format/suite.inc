<?php

/**
 * @file
 * The default format for adresses.
 */

$plugin = array(
  'title' => t('Suite'),
  'format callback' => 'addressfield_format_suite_generate',
  'type' => 'email',
  'weight' => -100,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_format_suite_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form') {
    $format['suite_block'] = array(
      '#type' => 'addressfield_container',
      '#attributes' => array('class' => array('custom-block')),
      '#weight' => -10,
    );
    $format['suite_block']['suite'] = array(
      '#type' => 'addressfield_container',
      '#attributes' => array('class' => array('addressfield-container-inline')),
    );
    $format['suite_block']['suite']['suite'] = array(
      '#title' => t('Suite'),
      '#size' => 10,
      '#attributes' => array('class' => array('suite')),
      '#type' => 'textfield',
      '#tag' => 'span',
      '#default_value' => isset($address['suite']) ? $address['suite'] : '',
    );
  }
  else {
    // Add our own render callback for the format view
    $format['#pre_render'][] = 'addressfield_suite_render_address';
  }
}

